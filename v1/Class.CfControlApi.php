<?php
namespace CfControl;
/**
* Třída CfControl/Api sloužící pro přístup k API systému Cf control (https://www.cf-control.cz)
* @package cf-control-api
* @author Cf Control <info@cf-control.cz>
* @version 1.31
*/
class Api {
	/**
	* URL API
	* @access private
	* @var string
	*/
	private $apiUrl = null;

	/**
	* API-KEY
	* @access private
	* @var string
	*/
	private $apiKey = null;

	/**
	* cURL resource
	* @access private
	* @var resource
	*/
	private $ch = null;

	/**
	* Timeout
	* @access private
	* @var int
	*/
	private $timeout = 2;

	/**
	* inicializační metoda třídy
	* @param string $apiUrl URL umístění API
	* @param string $apiKey API klíč k autorizaci
	*/
	function __construct($apiUrl, $apiKey) {
		if(!preg_match('/(\/$)/', $apiUrl)) {
			$apiUrl .= '/';
		}

		$this->apiUrl = $apiUrl;
		$this->apiKey = $apiKey;
	}

	/**
	* metoda pro přístup k api skrze GET požadavek
	* @access public
	* @param string $action požadavek, který se má vykonat
	* @param array $settings nastavení požadavku
	* @return array
	*/
	public function get($action, $settings = []) {
		$returnData = [
			'data' => [],
			'result' => 'CF_API_RESULT_ERROR',
			'error' => [],
		];

		if(empty($action) || !is_string($action)) {
			$returnData['error'][] = [
				'key' => 'CF_API_ERROR_INPUT_ACTION',
				'text' => 'Vstupní proměnná $action je ve špatném tvaru.',
			];
		}

		if(!is_array($settings)) {
			$returnData['error'][] = [
				'key' => 'CF_API_ERROR_INPUT_SETTINGS',
				'text' => 'Vstupní proměnná $settings je ve špatném tvaru.',
			];
		}

		if(!empty($returnData['error'])) {
			return $returnData;
		}

		$returnData['error'][] = [
			'key' => 'CF_API_ERROR_DATA',
			'text' => 'Nepovedlo se získat / odeslat data.',
		];

		$request = ['action' => $action, 'settings' => $settings];

		if($this->init()) {
			$this->buildQuery($request);

			$execData = $this->exec();

			if(!$this->isError()) {
				$_returnData = $this->parseData($execData);

				if($_returnData === NULL) {
					$returnData['error'][] = [
						'key' => 'CF_API_ERROR_JSON',
						'text' => 'JSON ERROR CODE: '.json_last_error(),
					];
				} else {
					$returnData = $_returnData;
				}
			} else {
				$returnData['error'][] = [
					'key' => 'CF_API_ERROR_CURL',
					'text' => 'cURL ERROR: '.$this->getError(),
				];
			}

			$this->close();
		}

		return $returnData;
	}

	/**
	* metoda pro přístup k api skrze POST požadavek
	* @access public
	* @param string $action požadavek, který se má vykonat
	* @param array $data data posílaná POST požadavkem
	* @return array
	*/
	public function post($action, $data) {
		$returnData = [
			'data' => [],
			'result' => 'CF_API_RESULT_ERROR',
			'error' => [],
		];

		if(empty($action) || !is_string($action)) {
			$returnData['error'][] = [
				'key' => 'CF_API_ERROR_INPUT_ACTION',
				'text' => 'Vstupní proměnná $action je ve špatném tvaru.',
			];
		}

		if(!is_array($data)) {
			$returnData['error'][] = [
				'key' => 'CF_API_ERROR_INPUT_DATA',
				'text' => 'Vstupní proměnná $data je ve špatném tvaru.',
			];
		}

		if(!empty($returnData['error'])) {
			return $returnData;
		}

		$returnData['error'][] = [
			'key' => 'CF_API_ERROR_DATA',
			'text' => 'Nepovedlo se získat/odeslat data.',
		];

		$request = ['action' => $action];

		if($this->init()) {
			$this->buildQuery($request);
			$this->buildPost($data);

			$execData = $this->exec();

			if(!$this->isError()) {
				$_returnData = $this->parseData($execData);

				if($_returnData === NULL) {
					$returnData['error'][] = [
						'key' => 'CF_API_ERROR_JSON',
						'text' => 'JSON ERROR CODE: '.json_last_error(),
					];
				} else {
					$returnData = $_returnData;
				}
			} else {
				$returnData['error'][] = [
					'key' => 'CF_API_ERROR_CURL',
					'text' => 'cURL ERROR: '.$this->getError(),
				];
			}

			$this->close();
		}

		return $returnData;
	}

	/**
	* metoda pro nastavení timeoutu požadavků
	* @access public
	* @param int $timeout timeout
	* @return boolean
	*/
	public function setTimeout($timeout) {
		$timeout = intval($timeout);

		if($timeout > 0) {
			$this->timeout = $timeout;
		}

		return $timeout > 0;
	}

	/**
	* privátní metoda pro převod dat na pole
	* @access private
	* @param array $data data v JSON formátu
	* @return array
	*/
	private function parseData($data) {
		return json_decode(str_replace('\xEF\xBB\xBF', '', $data), true);
	}

	/**
	* metoda pro sestavení GET dat
	* @access private
	* @param array $request nastavení GET požadavku
	* @return boolean
	*/
	private function buildQuery($request) {
		curl_setopt($this->ch, CURLOPT_URL, $this->apiUrl.'?'.http_build_query($request));

		return true;
	}

	/**
	* metoda pro sestavení POST dat
	* @access private
	* @param array $data data posílaná POST požadavkem
	* @return boolean
	*/
	private function buildPost($data) {
		curl_setopt($this->ch, CURLOPT_POST, true);
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, http_build_query($data));

		return true;
	}

	/**
	* metoda pro inicializaci cURL spojení
	* @access private
	* @return boolean
	*/
	private function init() {
		if($this->ch == null || !$this->ch){
			$this->ch = curl_init();

			if($this->ch) {
				curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, false);
				curl_setopt($this->ch, CURLOPT_TIMEOUT, $this->timeout);
				curl_setopt($this->ch, CURLOPT_HTTPHEADER, ['Cf-API-Authorization: '.$this->apiKey]);
			}
		}

		if($this->ch) {
			return true;
		}

		return false;
	}

	/**
	* metoda pro konec cURL spojení
	* @access private
	* @return boolean
	*/
	private function close() {
		if($this->ch) {
			curl_close($this->ch);

			$this->ch = null;
		}

		return true;
	}

	/**
	* metoda pro vykonání cURL požadavku
	* @access private
	* @return array|boolean
	*/
	private function exec() {
		if($this->ch) {
			return curl_exec($this->ch);
		}

		return false;
	}

	/**
	* metoda pro získání cURL erroru
	* @access private
	* @return string|boolean
	*/
	private function getError() {
		if($this->ch) {
			return curl_error($this->ch);
		}

		return false;
	}

	/**
	* metoda pro získání informace o erroru
	* @access private
	* @return boolean
	*/
	private function isError() {
		if($this->ch) {
			return curl_errno($this->ch) == 0 ? false : true;
		}

		return false;
	}

	/**
	* metoda spouštěná pomocí Garbage Collection
	*/
	function __destruct() {
		if($this->ch) {
			curl_close($this->ch);
		}
	}
}