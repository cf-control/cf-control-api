<?php
header('Content-Type: text/html; charset=utf-8');

include __DIR__.'/Class.CfControlApi.php';

use CfControl\Api;
$api = new Api('https://demo.cf-control.cz/api/web/v1/', 'veyg<!hR]sbG*w:r;NtQ,vh5aQVAT?');

/* vypsání všech veřejných tarifů, minimální data */
print_r($api->get('getTarifList', ['tarifDataType' => 'minimal']));

/* vypsání všech novinek */
print_r($api->get('getNews'));

/* vypsání všech souborů */
print_r($api->get('getFiles'));

/* vypsání všech nepřečtených příchozích SMS zpráv; označení jako přečtené */
print_r($api->get('getIncomingSMS', ['itemsType' => 'unread', 'markAsRead' => 1]));

/* vložení nové odchozí SMS zprávy */
print_r($api->post(
	'addSMS',
	[
		'PhoneNumber' => 123456789,
		'Text' => 'Testovací SMS z API.',
	]
));
print_r($api->post(
	'addSMS',
	[
		'PhoneNumber' => '+420123456789',
		'Text' => 'Testovací SMS z API.',
	]
));

/* vložení zájemce o internet, osoba */
print_r($api->post(
	'addCandidates',
	[
		'IsCompany' => 0, // firma = NE
		'Name' => 'Lukáš', // jméno,
		'Surname' => 'Sieber', // příjmení
		'AddressEstablishmentStreet' => 'Ulice', // ulice
		'AddressEstablishmentNumber' => '133', // číslo popisné
		'AddressEstablishmentObjectType' => 0, // typ domu = rodinný dům
		'AddressEstablishmentCity' => 'Mnichovo Hradiště', // město
		'AddressEstablishmentZip' => '295 01', // PSČ
		'Phone' => '+420 776 883 744', // telefon
		'Email' => 'info@cf-control.cz', // email
		'Tarif' => 16, // ID tarifu
		'ContractLength' => 0, // délka smlouvy = neurčito
		'OwnEquipment' => 1, // vlastní zařízení = ANO
		'Description' => '', // poznámka
	]
));

/* vložení zájemce o internet, firma */
print_r($api->post(
	'addCandidates',
	[
		'IsCompany' => 1, // firma = ANO
		'Name' => 'Lukáš', // jméno
		'Surname' => 'Sieber', // příjmení
		'Company' => 'Cf Control', // název firmy
		'CompanyIco' => '12345678', // IČO
		'CompanyDic' => '', // DIČ, nepovinné, bez CZ-, SK-
		'CompanyVatPayer' => 0, // plátce DPH, pouze pro Slovensko (pro IČDPH)
		'AddressEstablishmentStreet' => 'Ulice', // ulice
		'AddressEstablishmentNumber' => '133', // číslo popisné
		'AddressEstablishmentObjectType' => 1, // typ domu = panelový dům
		'AddressEstablishmentFloor' => 1, // patro, pouze v případě panelového domu
		'AddressEstablishmentFlatNumber' => 23, // číslo bytu, pouze v případě panelového domu
		'AddressEstablishmentCity' => 'Mnichovo Hradiště', // město
		'AddressEstablishmentZip' => '295 01', // PSČ
		'AddressBillingStreet' => 'U kostela', // fakturační ulice
		'AddressBillingNumber' => '13', // fakturační číslo popisné
		'AddressBillingCity' => 'Mladá Boleslav', // fakturační město
		'AddressBillingZip' => '293 01', // fakturační PSČ
		'Phone' => '776883744', // telefon
		'Email' => 'info@cf-control.cz', // email
		'Tarif' => 16, // ID  tarifu
		'ContractLength' => 24, // délka smlouvy = 24 měsíců
		'OwnEquipment' => 0, // vlastní zařízení = NE
		'Description' => 'Poznámka k uživateli.', // poznámka
	]
));

/* vložení nové faktury */
print_r($api->post(
	'insertInvoice',
	[
		'customerId' => 2,
		'invoiceNumberQueue' => 118,
		'invoiceNumberCount' => 6,
		'invoiceNumber' => 1,
		'date' => date('d.m.Y'),
		'paymentType' => 'bank',
		'maturity' => 14,
		'priceType' => 1,
		'items' => [
			['name' => 'Test', 'amount' => 1.25, 'unit' => 'hod', 'price' => 25.52, 'sale' => 10, 'saleInPrice' => 0],
		],
	]
));